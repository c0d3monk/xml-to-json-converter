# xml-json-converter

An utility tool written in Perl to convert XML to JSON format

## Install

* Download: [click here](https://bintray.com/akr-optimus/deb/download_file?file_path=pool%2Fx%2Fxml-to-json-converter%2Fxml-to-json-converter.deb)
or `curl -O "https://dl.bintray.com/akr-optimus/deb/pool/x/xml-to-json-converter/xml-to-json-converter.deb"`

## Execute

* `perl /usr/bin/xml2json.pl --input=<path to xml file> [--outputdir=<>] [--outputfilename=<>]`
