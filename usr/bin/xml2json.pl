#!/usr/bin/env perl

use strict;
use warnings;
use XMLToJSONConverter;
use Getopt::Long;
use Carp;


my %options;

GetOptions(\%options, "input|i=s", "outputdir=s", "outputfilename=s");

my $usage = "perl xml2json.pl --input=<path to xml file> [outputdir=<>] [outputfilename=<>]";

unless ($options{'input'}) {
    confess $usage;
}

my $obj = XMLToJSONConverter->new(%options);
$obj->transform();


