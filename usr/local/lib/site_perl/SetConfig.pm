package SetConfig;
use strict;
use warnings FATAL => 'all';
use Carp;

sub new {
    my $class = shift;
    my %args = @_;
    my $self = {};
    bless $self, $class;
    $self->setconfig(%args);
    return $self;

}

sub setconfig {
    my $self = shift;
    my %args = @_;
    $self->{input} = $args{input};
    if ($args{'input'} =~ /.*\.xml$/i) {
        $self->{output_filename} = 'result.json';
    } elsif ($args{'input'} =~ /.*\.json$/i) {
        $self->{output_filename} = 'result.xml';
    } else {
        confess "Can't determine input file type";
    }
    $self->{output_dir} = $args{'outputdir'} || './';
}

1;