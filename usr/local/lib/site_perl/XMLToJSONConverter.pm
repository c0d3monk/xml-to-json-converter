package XMLToJSONConverter;

use XML::LibXML;
use JSON;
use Carp;
use strict;
use warnings;
use Data::Dumper;
use SetConfig;

sub new {
    my $class = shift;
    my $self = {};
    my %args = ();
    if (ref($_[0]) eq 'HASH') {
        %args = %{$_[0]};
    } else {
        %args = @_;
    }
    bless $self, $class;
    $self->{cfg} = SetConfig->new(%args);
    return $self;
}

sub print_to_file {
    my $self = shift;
    my $obj = shift;
    open (my $fh, ">", $self->{cfg}->{output_dir} . '/'. $self->{cfg}->{output_filename});
    print $fh $obj;
    close $fh;
    return;
}

sub transform {
    my $self = shift;
    my $parser = XML::LibXML->new();
    my $doc = $parser->parse_file($self->{cfg}->{input});
    my $obj = $self->convert_dom($doc);
    my $json = JSON->new();
    my $encoded_json = $json->pretty->encode($obj);
    $self->print_to_file($encoded_json);
    return;
}

sub convert_dom {
    my $self = shift;
    my $doc = shift;
    my $root = $doc->documentElement();
    my $node_name = $root->nodeName();
    my $doc_version = $doc->version;
    my $doc_encoding = $doc->encoding;
    my $obj = {};
    my $root_obj = {'version' => $doc_version, encoding => $doc_encoding, $node_name => $obj};

    my $text = $root->findvalue('text()');
    $text = undef unless $text =~ /\S/;
    $obj->{content} = $text if defined($text);

    my @attributes = $root->findnodes('@*');
    if (@attributes) {
        foreach my $attr (@attributes) {
            my $attrname = $attr->nodeName();
            my $attrvalue = $attr->nodeValue();
            $obj->{$attrname} = $attrvalue;
        }
    }

    my @namespaces = $root->getNamespaces();
    if (@namespaces) {

        foreach my $namespace (@namespaces) {
            my $prefix = $namespace->declaredPrefix();
            my $uri = $namespace->declaredURI;
            $prefix = ":$prefix" if $prefix;
            $obj->{'xmlns' . $prefix} = $uri;
        }
    }

    $self->parse_children($root, $obj);

    return $root_obj;
}

sub parse_children {
    my $self = shift;
    my $element = shift;
    my $obj = shift;

    my @children = $element->findnodes('*');
    if (@children) {
        foreach my $child (@children) {
            my $element_struct = {};
            my $nodename = $child->nodeName();
            if (exists $obj->{$nodename}) {
                my $type = ref ($obj->{$nodename});
                if ($type eq 'HASH') {
                    $obj->{$nodename} = [$obj->{$nodename}];
                }
                if ($type eq '') {
                    $obj->{$nodename} = [];
                }
                push @{$obj->{$nodename}}, $element_struct;
            } else {
                $obj->{$nodename} = $element_struct ;
            }
            my $txt = $child->findvalue('text()');
            $txt = undef unless $txt =~ /\S/;
            $element_struct->{content} = $txt if defined($txt);

            my @attrs = $child->findnodes('@*');
            if (@attrs) {
                foreach my $attr (@attrs) {
                    my $attrname = $attr->nodeName();
                    my $attrvalue = $attr->nodeValue();
                    $element_struct->{$attrname} = $attrvalue;
                }
            }
            my @namespaces = $child->getNamespaces();
            if (@namespaces) {
                foreach my $namespace (@namespaces) {
                    my $px = $namespace->declaredPrefix;
                    my $uri = $namespace->declaredURI;
                    $px = ":$px" if defined($px);
                    $element_struct->{'xmlns'.$px} = $uri;
                }
            }
            $self->parse_children($child, $element_struct);
        }
    }
    return;

}


1;
